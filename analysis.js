var $ = $ || {}, d3 = d3 || {};
var analysis, dimensions;

function s2ia(s){
    return Array.from(s).map(function (x) { return parseInt(x, 10); });
}

function get_analysis(v, n, p, k, t){
	var key = [v,n,k,p].join('/');
	return (key in analysis)?analysis[key][t]:NaN;
}

function validate_dimensions(dim, dict){
    var r = [], total=0, miss=0;
    for(var v of dim.V)for(var n of dim.N)for(var p of dim.P)for(var k of dim.K){
        var key = [v,n,k,p].join('/');
        if(!(key in dict)) { r.push(key); ++miss; }
        ++total;
    }
    return {total: total, coverage: 1-(miss/total), result: r};
}

function get_speedup_v_p(){
	const n=8400, k=500;
	var r=[];
	for(var v of dimensions.V) {
		var base_t = get_analysis(v,n,1,k,'total_ms');
		var data_points = new Array();
		r.push(data_points);
		data_points.push({x:dimensions.P[0], y:1});
		for(var p of dimensions.P) if(p>1) {
			var _y = base_t/get_analysis(v,n,p,k,'total_ms');
			if(isNaN(_y)) data_points.push(null);
			else data_points.push({x:p, y:_y});
		}
	}
	return r;
}

$.getJSON("analysis.json" , function(r){
	analysis = r;
	var r_key = /(.+)\/(\d+)\/(\d+)\/(\d+)/, vnpk = [new Set, new Set, new Set, new Set];
	for(var i in analysis){
		var m = r_key.exec(i);
		for(var k=0;k<4;++k) vnpk[k].add(m[k+1]);
	}
	dimensions = {
		V: Array.from(vnpk[0]).sort(),
		N: s2ia(vnpk[1]).sort((a, b) => (a - b)),
		P: s2ia(vnpk[3]).sort((a, b) => (a - b)),
		K: s2ia(vnpk[2]).sort((a, b) => (a - b)),
	};
	console.log(dimensions);
	console.log(validate_dimensions(dimensions, analysis));
	
	$(render_plots);
});

function render_plots() {
	var view = { width: 1320, height: 750 };
	
	var svg = d3.select("body").append("svg")
		.attr("width", view.width)
		.attr("height", view.height);
	var h_hist, h_plot_pt, h_plot_kt, h_plot_nt, h_timeline, h_report; // plot handles
		    
	var color_scale_v = d3.scaleLinear()
	    .domain([0, 9])
	    .range(["steelblue", "red"])
	    .interpolate(d3.interpolateLab);
	var speedup_v_p = get_speedup_v_p();
	
	function render_plot0(margin){
	    var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		
		var x = d3.scaleLinear()
			.domain([0.5,9])
		    .range([0, width]);
		
		var y = d3.scaleLinear()
			.domain([0,8])
		    .range([height, 0]);
		
		var line = d3.line()
		    .defined(function(d) { return d; })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		
		container.append("g").selectAll(".line")
		    .data(speedup_v_p)
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", function(d, i) { return color_scale_v(i); } )
		    .attr("d", line);
		
		for(var dots_v=0;dots_v<speedup_v_p.length;++dots_v){
			container.selectAll(".dot .dot"+dots_v)
			    .data(speedup_v_p[dots_v].filter(function(d) { return d; }))
				.enter().append("circle")
			    .attr("class", "dot")
			    .attr("stroke", function(d, i) { return color_scale_v(dots_v); } )
			    .attr("cx", line.x())
			    .attr("cy", line.y())
			    .attr("r", 3.5);
		}
		
		var group_legend = container.append("g");
	
		var group_legend_pointers = group_legend.append("text")
			.text("<<")
			.style("font-size","20px")
			.attr("x", function(d) { return x(8.2); });
		
		group_legend.selectAll(".legend")
			.data(dimensions.V)
			.enter().append("rect")
			.attr("class","square")
			.attr("x", function(d) { return 20; })
			.attr("y", function(d, i) { return 20*i; })
			.attr("width", function(d) { return 16; })
			.attr("height", function(d) { return 16; })
			.style("fill", function(d, i) { return color_scale_v(i); })
			.style("stroke", "#000")
			.style("stroke-width", ".5px")
			.on('mouseover', function(d, i) {
				// console.log(dimensions.V[i]);
				if(h_hist) h_hist.remove();
				group_legend_pointers.attr("y", y(speedup_v_p[i][4].y-0.2));
				h_hist = render_plot_hist({top: 350, right: 660, bottom: 40, left: 40}, dimensions.V[i], 8400);
			});
			
		group_legend.selectAll(".legend")
			.data(dimensions.V)
			.enter().append("text")
			.attr("x", function(d) { return 20+18; })
			.attr("y", function(d, i) { return 20*i+8; })
			.attr("width", function(d) { return 16; })
			.attr("height", function(d) { return 16; })
			.text(function(d) { return d; })
			.style("stroke", "#000")
			.style("stroke-width", ".5px");
	}
	
	function render_plot_hist(margin, version, n){
	    var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		var gridData = new Array, cell_width = 32;
		var grid_xpos = 0, grid_ypos = 0;
		for(var k of dimensions.K){
			var rowData = new Array;
			grid_xpos = 0;
			for(var p of dimensions.P){
				rowData.push({
					val: get_analysis(version,n,p,k,"total_ms"),
					x: grid_xpos, y: grid_ypos,
					v: version, n:n, p:p, k:k,
					width: cell_width//p<=8?cell_width:12
				});
				grid_xpos+=cell_width;
			}
			gridData.push(rowData);
			grid_ypos+=cell_width;
		}

		var row = container.selectAll(".row")
			.data(gridData)
			.enter().append("g")
			.attr("class", "row");
			
		var color_scale_t = d3.scaleLinear()
		    .domain([0, 80])
		    .range(["green", "red"])
		    .interpolate(d3.interpolateLab);
			
		row.selectAll(".square")
			.data(function(d) { return d; })
			.enter().append("rect")
			.attr("class","square")
			.attr("x", function(d) { return d.x; })
			.attr("y", function(d) { return d.y; })
			.attr("width", function(d) { return d.width; })
			.attr("height", function(d) { return cell_width; })
			.style("fill", function(d){
				if(isNaN(d.val)) return "#fff"; 
				else return color_scale_t(d.val);
			})
			.style("stroke", "#000")
			.style("stroke-width", ".5px")
			.on('mouseover', function(d, i) {
				// console.log(d);
				if(h_plot_pt) h_plot_pt.remove();
				if(h_plot_kt) h_plot_kt.remove();
				if(h_plot_nt) h_plot_nt.remove();
				if(h_timeline) h_timeline.remove();
				if(h_report) h_report.remove();
				var top_right = {top: 340, right: 680, bottom: 240, left: 360},
					bottom_right = {top: 520, right: 680, bottom: 40, left: 360},
					bottom_left = {top: 520, right: 1000, bottom: 40, left: 40};
				h_plot_pt = render_plot_pt(top_right, d.v, d.n, d.k);
				h_plot_kt = render_plot_kt(bottom_left, d.v, d.n, d.p);
				h_plot_nt = render_plot_nt(bottom_right, d.v, d.p, d.k);
				h_timeline = render_timeline({top: 20, right: 40, bottom: 340, left: 720}, d.v, d.n, d.p, d.k);
				h_report = render_report({top: 460, right: 20, bottom: 20, left: 690}, d.v, d.n, d.p, d.k);
			});
		
		return container;
	}
	
	function render_plot_pt(margin, version, n, k){
		var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		var x = d3.scaleLinear()
			.domain([0.5,9])
		    .range([0, width]);
		var y = d3.scaleLinear()
			.domain([1,100])
		    .range([height, 0]);
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		
		var line = d3.line()
		    .defined(function(d) { return !isNaN(d.y); })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		var data_points = new Array;
		for(var p of dimensions.P)
			data_points.push({ x: p, y: get_analysis(version,n,p,k,"total_ms")});
		container.append("g").selectAll(".line")
		    .data([data_points])
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", "blue" )
		    .attr("d", line);
		   
		return container;
	}
	
	function render_plot_kt(margin, version, n, p){
		var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		var x = d3.scaleLinear()
			.domain([100,2000])
		    .range([0, width]);
		var y = d3.scaleLinear()
			.domain([1,100])
		    .range([height, 0]);
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		    
		var line = d3.line()
		    .defined(function(d) { return !isNaN(d.y); })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		var data_points = new Array;
		for(var k of dimensions.K)
			data_points.push({ x: k, y: get_analysis(version,n,p,k,"total_ms")});
		container.append("g").selectAll(".line")
		    .data([data_points])
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", "blue" )
		    .attr("d", line);
		    
		return container;
	}
	
	function render_plot_nt(margin, version, p, k){
		var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		var x = d3.scaleLinear()
			.domain([2000,8500])
		    .range([0, width]);
		var y = d3.scaleLinear()
			.domain([1,100])
		    .range([height, 0]);
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		    
		var line = d3.line()
		    .defined(function(d) { return !isNaN(d.y); })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		var data_points = new Array;
		for(var n of dimensions.N)
			data_points.push({ x: n, y: get_analysis(version,n,p,k,"total_ms")});
		container.append("g").selectAll(".line")
		    .data([data_points])
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", "blue" )
		    .attr("d", line);
		
		return container;
	}
	
	function render_timeline(margin, version, n, p, k){
		var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		var x = d3.scaleLinear()
			.domain([0,100])
		    .range([0, width]);
		function _child(n) {return "child["+n+"]";}
		var labels = ["partition"];
		for(var chldi=0;chldi<8;++chldi) labels.push(_child(chldi));
		labels.push("merge");
		var y = d3.scaleBand()
	      .domain(labels)
	      .rangeRound([0, height])
	      .padding(0.1);
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		function _get(t){return get_analysis(version, n, p, k, t);}
		var _children_start = _get("children_start"), _children_end = _get("children_end");
		var data_points = [
			{s:_get("partition_start"),e:_get("partition_end"),label:"partition"},
			{s:_children_start[0],e:_children_end[0],label:_child(0)},
			{s:_children_start[1],e:_children_end[1],label:_child(1)},
			{s:_children_start[2],e:_children_end[2],label:_child(2)},
			{s:_children_start[3],e:_children_end[3],label:_child(3)},
			{s:_children_start[4],e:_children_end[4],label:_child(4)},
			{s:_children_start[5],e:_children_end[5],label:_child(5)},
			{s:_children_start[6],e:_children_end[6],label:_child(6)},
			{s:_children_start[7],e:_children_end[7],label:_child(7)},
			{s:_get("merge_start"),e:_get("merge_end"),label:"merge"},
		].filter(function(d){return !isNaN(d.s)});
		container.append("g").selectAll("rect")
	      .data(data_points).enter().append("rect")
	      .attr("class", "rect")
	      .attr("y", function(d) { return y(d.label); })
	      .attr("x", function(d) { return x(d.s); })
	      .attr("width", function(d) { return x(d.e) - x(d.s); })
	      .attr("height", y.bandwidth());
      
		return container;
	}
	
	function render_report(margin, version, n, p, k){
		var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		container.append("text")
			.text("version:"+version+" n:"+n+" p:"+p+" k:"+k)
			.style("font-size","20px");
			
		function _get(t){return get_analysis(version, n, p, k, t);}
		container.append("text")
			.text("parse:"+_get("parse_s")+"s partition:"+_get("partition_ms")+"ms merge:"+_get("merge_ms")+"ms total:"+_get("total_ms"))
			.attr("y",30)
			.style("font-size","20px");
			
		container.append("text")
			.text("user:"+_get("user_s")+"s sys:"+_get("sys_s")+"s cpu:"+_get("cpu_%")+"% mem:"+_get("mem_kbytes")+"kB")
			.attr("y",60)
			.style("font-size","20px");
			
		container.append("text")
			.text("context switch:"+_get("cs_i")+"(involuntary) "+_get("cs_v")+"(voluntary)")
			.attr("y",90)
			.style("font-size","20px");
			
			
			
			/*
			
			parse_s partition_ms merge_ms total_ms
			user_s sys_s cpu_% mem_kbytes
			cs_i cs_v
			
			*/
			
			
			
		// container.append("rect")
	 //     .attr("class", "rect")
	 //     .attr("y", 0)
	 //     .attr("x", 0)
	 //     .attr("width", width)
	 //     .attr("height", height);
		return container;
	}
	
	render_plot0({top: 20, right: 660, bottom: 440, left: 40});
}

