"""Usage: python extract.py ./hpchw{2,3} > analysis.json"""
import os, re, sys, itertools, linecache, json
parse = lambda pattern, array, ctor: map(ctor, filter(bool, map(re.compile(pattern).match, array)))
scan = lambda fname: itertools.takewhile(lambda l:len(l), (linecache.getline(fname, i) for i in itertools.count(1)))
print json.dumps(dict(parse(r'^\./.*(V[^/]+)/(\d+)/log\.K=(\d+),P=(\d+)$', 
    (os.path.join(root,file) for root, dirs, files in itertools.chain.from_iterable(os.walk(path) for path in sys.argv[1:]) for file in files),
    lambda m: ('/'.join(m.groups()), (lambda (f,a):{
        'parse_s': f('File parsed in (.*)s'),
        'partition_ms': f('Partitioned in (.*)ms'), 'merge_ms': f('Merged in (.*)ms'),
        'total_ms': f('Total time finding the vector (.*)ms'),
        'user_s': f(r'\s*User time.*: (.*)'), 'sys_s': f(r'\s*System time.*: (.*)'),
        'cpu_%': f(r'\s*Percent of CPU.*: (.*)%'), 'mem_kbytes': f(r'\s*Maximum resident set size.*: (.*)'),
        'cs_i': f(r'\s*Involuntary context switches: (.*)'), 'cs_v': f(r'\s*Voluntary context switches: (.*)'),
        'children_ms': a('children.* finished in (.*)ms'), 'children_lines': a(r'children.* finished.*\((\d+)lines'),
        'children_start': a(r'children.* finished in.*\[(.*),'), 'children_end': a(r'children.* finished in.*,(.*)\]'),
        'partition_start': f('Partitioned in.*\[(.*),'), 'partition_end': f('Partitioned in.*,(.*)\]'),
        'merge_start': f('Merged in.*\[(.*),'), 'merge_end': f('Merged in.*,(.*)\]'),
    })((lambda fname: (
        lambda pattern: parse(pattern, scan(fname), lambda m: float(m.group(1)))[0], # float scanner
        lambda pattern: parse(pattern, scan(fname), lambda m: float(m.group(1))) # float array scanner
))(m.group(0)))))), separators=(',',':'))