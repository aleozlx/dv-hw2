# Visualization of 900+ log files in one page
Hover mouse on _boxes_ (e.g. versions, histogram) to view *interactively*.
Something I put together in 7hrs, not well documented at all, hence a little explanation:

## Problem
Search for top K most similar vectors from N of those in total with P threads/processes.

## Structure
{(Version, N, P, K) => (9 performance metrics)}

## Graphs

Left column
* Relative Speedup v. P (Show linearity of speedup of each version)
* P-K 2D histogram (x-axis:P y-axis:K; color scale:[Red to Green]=>[More time to less time])
* P-t K-t N-t trending

Right column
* Timeline
* Other metrics

